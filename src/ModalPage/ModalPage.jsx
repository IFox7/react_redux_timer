import React, { useRef } from "react";

import "./modalPage.css";
import { useDispatch } from "react-redux";
// Модальне вікно для відображення попередньо заданих таймерів
export default function ModalPage({ classModal }) {
  const dispatch = useDispatch(),
    modalRef = useRef(),
    // Функція закриття модального вікна
    closeModal = () => {
      dispatch({ type: "Modal", payload: false });
      modalRef.current.classList.add("visible");
    },
    // Функція очистки локал сторіджа від інфо про попередні таймери з закриттям мод.вікна
    clearStorage = () => {
      localStorage.removeItem("previousSettings");
      dispatch({ type: "Modal", payload: false });
    };

  return (
    <div ref={modalRef} className={"modalWrapper " + classModal}>
      <div className="sessionTimerWrapper">
        <button className="closeModal" onClick={closeModal} type="button">
          ❌
        </button>
        <button className="cleareStorage" onClick={clearStorage} type="button">
          Очистити таймери✂
        </button>
        <h1>Інформація про встановлювані таймери</h1>
        <table>
          <thead>
            <tr>
              <th>Номер таймера</th>
              <th>Встановлені хвилини</th>
              <th>Встановлені секунди</th>
            </tr>
          </thead>
          <tbody>
            {JSON.parse(localStorage.getItem("previousSettings"))?.map(
              (elem, ind) => {
                return (
                  <tr key={ind}>
                    <td>{ind}</td>
                    <td>{elem.minutes}</td>
                    <td>{elem.secondes}</td>
                  </tr>
                );
              }
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}

import React, { useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import SelectSound from "../SelectSound";
import "./timerControls.css";

import sound from "../../Sound/tik-tajmer.mp3";
import soundStop from "../../Sound/stop-golos.mp3";
import soundKids from "../../Sound/kids_sound.mp3";

// Елемент таймера з кнопками управління
export default function TimerControls({ refContainer }) {
  // Отримання станів таймера
  const minutes = useSelector((state) => state.minutes),
    secondes = useSelector((state) => state.secondes),
    startTime = useSelector((state) => state.start),
    soundChecked = useSelector((state) => state.sound),
    dispatch = useDispatch();
  let previousSettings = [];

  //   Отримання доступу для управління тегами проекту
  const btnStartRef = useRef(),
    soundRef = useRef(),
    soundStopRef = useRef(),
    soundFinishRef = useRef();

  // Функція встановлення початкової точки відліку таймера
  const setTime = (e) => {
    dispatch({
      type: "SetTimer",
      payload: {
        minutes: e.target.value.split(":")[0],
        secondes: e.target.value.split(":")[1],
      },
    });
    btnStartRef.current.disabled = false;
  };

  //   Функція запуску таймера
  const launchTimer = () => {
    btnStartRef.current.disabled = true;
    if (minutes >= 0 && secondes !== 0) {
      document.body.classList.add("dark");
      refContainer.current.classList.add("darkMode");
      dispatch({ type: "StopTimer", payload: true });
    }
    const start = setInterval(() => {
      dispatch({
        type: "StartTimer",
        payload: { minutes: 1, secondes: 1, start: start },
      }) +
        +localStorage.getItem("currentMinute") !==
        0 && +localStorage.getItem("currentSecond") !== 0
        ? soundRef.current.play()
        : soundRef.current.pause();
      if (
        +localStorage.getItem("currentMinute") === 0 &&
        +localStorage.getItem("currentSecond") === 1
      ) {
        clearInterval(start);
        dispatch({ type: "StopTimer", payload: false });
        localStorage.setItem("minutes", 0);
        localStorage.setItem("secondes", 0);
        soundRef.current.pause();
        soundFinishRef.current.play();
        document.body.classList.remove("dark");
        refContainer.current.classList.remove("darkMode");
      }
    }, 1000);
    JSON.parse(localStorage.getItem("previousSettings"))
      ? (previousSettings = JSON.parse(
          localStorage.getItem("previousSettings")
        ))
      : (previousSettings = []);
    previousSettings.push({ minutes, secondes });
    localStorage.setItem("previousSettings", JSON.stringify(previousSettings));
  };

  // Функція зупинки таймера
  const stop = () => {
    dispatch({ type: "StopTimer", payload: false });
    soundRef.current.pause();
    clearInterval(startTime);
    localStorage.setItem("minutes", minutes);
    localStorage.setItem("secondes", secondes);
    soundStopRef.current.play();
    btnStartRef.current.disabled = false;
  };

  return (
    <>
      <SelectSound />
      <div className="setTimerWrapper">
        <label htmlFor="setterTime">Встановити час</label>
        <input
          onChange={(e) => (e.target.id === "setterTime" ? setTime(e) : 0)}
          id="setterTime"
          type="time"
        />
      </div>
      <div className="btnWrapper">
        <input
          ref={btnStartRef}
          type="button"
          onClick={launchTimer}
          value="Start"
          className="btn"
        />
        <input type="button" onClick={stop} value="Stop" className="btn" />
      </div>
      <audio ref={soundRef} src={sound} />
      <audio ref={soundStopRef} src={soundStop} />
      <audio
        ref={soundFinishRef}
        src={soundChecked ? soundChecked : soundKids}
      />
    </>
  );
}

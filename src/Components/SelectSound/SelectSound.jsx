import React, { useRef } from "react";

import { useDispatch } from "react-redux";

import soundKids from "../../Sound/kids_sound.mp3";
import soundClaps from "../../Sound/claps_sound.mp3";
import soundFinish from "../../Sound/finish_sound.mp3";
import soundGong from "../../Sound/gong_sound.mp3";

export default function SelectSound() {
  const dispatch = useDispatch(),
    selectRef = useRef();

  //   Функція вибору звука при закінченні відліку таймера
  const SoundChoose = () => {
    dispatch({ type: "SoundCheck", payload: selectRef.current.value });
  };

  return (
    
    <div className="soundChooseWrapper">
    <label htmlFor="soundChoose">Вибір сигналу фініша</label>
    <select
      ref={selectRef}
      onChange={SoundChoose}
      name="Sound"
      id="soundChoose"
    >
      <option value={soundKids}>Kids Cheering</option>
      <option value={soundClaps}>Sound Claps</option>
      <option value={soundFinish}>Electronic Sound</option>
      <option value={soundGong}>Sound Gong</option>
    </select>
    </div>
    
  );
}

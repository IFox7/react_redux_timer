import React, { useRef } from "react";

import "./timer.css";

import TimerDisplay from "../TimerDisplay";
import TimerControls from "../TimerControls/TimerControls";
import ModalPage from "../../ModalPage/ModalPage";
import { useDispatch, useSelector } from "react-redux";
// Таймер зі всіма компонентами
export default function Timer() {
  const dispatch = useDispatch(),
    modalWindow = useSelector((state) => state.modal);
  let classModal = modalWindow ? "" : "visible";
  // Функція виклику модального вікна ,що відображує всі попередньо запущені таймери
  const modalActivate = () => {
    dispatch({ type: "Modal", payload: true });
  };
  const containerRef = useRef();
  return (
    <>
      <ModalPage classModal={classModal} />
      <div ref={containerRef} className="timerContainer">
        <TimerDisplay />
        <TimerControls refContainer={containerRef} />
        <input
          type="button"
          onClick={modalActivate}
          value="Інфо"
          className="btn btnInfo"
        />
      </div>
    </>
  );
}

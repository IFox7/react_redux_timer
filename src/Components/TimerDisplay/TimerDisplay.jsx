import React, { useRef } from "react";
import { useSelector } from "react-redux";
import "./timerDisplay.css";

// Елемент дисплея таймера
export default function TimerDisplay() {
  const minutes = useSelector((state) => state.minutes),
    secondes = useSelector((state) => state.secondes),
    launch = useSelector((state) => state.launch),
    refSec = useRef(),
    ref = useRef(),
    displayRef = useRef();

  // Передаєм в локал сторідж поточний стан дисплея таймера
  refSec.current !== undefined
    ? localStorage.setItem("currentSecond", +refSec.current.innerText - 1)
    : localStorage.setItem("currentSecond", 0);
  ref.current !== undefined
    ? localStorage.setItem("currentMinute", ref.current.innerText)
    : localStorage.setItem("currentMinute", 0);
  return (
    <div
      ref={displayRef}
      className={launch ? "displayWrapper displayDarkMode" : "displayWrapper"}
    >
      <div ref={ref} className="minutesTimer">
        {minutes > 9 ? minutes : "0" + minutes}
      </div>
      <div className="dotsTimer">:</div>
      <div ref={refSec} className="secondsTimer">
        {secondes > 9 ? secondes : "0" + secondes}
      </div>
    </div>
  );
}
